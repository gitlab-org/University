# GitLab 8.2

### Big, Bigger, LFS

---

# Award Emoji

![inline](images/emoji.png)

---

# Releases

![inline](images/releases.png)

---

# Repository Mirroring (EE only)

![inline](https://gitlab.com/gitlab-org/gitlab-ee/uploads/f0f2e822133c5efd467fc0a173c6a8b7/Screen_Shot_2015-11-12_at_12.48.15.png)

---

# Global Milestones

![inline](images/gm.png)

---

# Build Artifacts

### Build output (binaries/data/documents) directly in GitLab

```
artifacts:
  paths:
  - binaries/
  - .config
```

&

```
artifacts:
  untracked: true
```

---

# Runner Caching

### Potentially much faster builds with CI

```
rspec:
  script: test
  cache:
    untracked: true
```

---

# One more thing..
