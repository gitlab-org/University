<html>
<head>
<title>Redirecting...</title>
<link rel="canonical" href="https://about.gitlab.com/handbook/sales-onboarding"/>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="refresh" content="0; url=https://about.gitlab.com/handbook/sales-onboarding" />
</head>
<body>
  <p><strong>Redirecting...</strong></p>
  <p><a href='https://about.gitlab.com/handbook/sales-onboarding'>Click here if you are not redirected.</a></p>
  <script>
    document.location.href = "https://about.gitlab.com/handbook/sales-onboarding";
  </script>
</body>
</html>
