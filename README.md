# Deprecated - Add Content to GitLab CE Documentation

GitLab University has been moved to [GitLab CE Documentation](https://gitlab.com/gitlab-org/gitlab-ce/tree/master/doc/university).

If you're interested in contributing, take a look at the updated [Process Article](https://gitlab.com/gitlab-org/gitlab-ce/tree/master/doc/university/process) for information on contributing.
